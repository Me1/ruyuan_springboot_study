package com.shangye.feignService;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 天元开放api
 * 采用feignClient 远程调用
 *
 * 熔断处理默认
 */

@FeignClient(name = "tyPublicApi",url="http://192.168.2.124:9090")
public interface TYPublicService {

    @RequestMapping(value = "/api/public/rwo",method = RequestMethod.GET)
    Map getRepairInfo(@RequestParam("repairNo") String repairNo);

    @RequestMapping(value = "/api/public/error",method = RequestMethod.GET)
    Map errorTest();
}
