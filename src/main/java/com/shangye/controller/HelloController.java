package com.shangye.controller;

import com.shangye.feignService.TYPublicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class HelloController {

    @Autowired
    private TYPublicService tyPublicService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/sayHello")
    public String HelloWorld(){
        return "hello world aa!";
    }

    @GetMapping("/repair/{repairNo}")
    public Map getRepairInfo(@PathVariable("repairNo") String repairNo){
        return tyPublicService.getRepairInfo(repairNo);
    }

    @GetMapping("/errorTest")
    public Map errorTest(){
        return tyPublicService.errorTest();
    }

    @PutMapping("/save")
    public void saveMap(@RequestParam Map param){
        System.out.println(param.keySet().size());
    }

    @GetMapping("/test")
    public void test(){
        jdbcTemplate.queryForList("select * from user");

    }
}
